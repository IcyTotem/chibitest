# What is it? #
This is an exercise to teach myself Unity5, or more precisely to get back into Unity as I had already programmed for the platform already. It is a very simple 3D free-camera game with a single level. The goal of the level is to find a way to leave the island. It encompasses the most basic fundamentals of game programming, such as animators with state machines, gizmos, enemy scripting, physics manipulation, collision handling, level design, UI design, etc…

### Disclaimer ###
I am not sure whether all assets necessary to build the game are included. If some is missing, please contact me.

### Assets ###
All the assets included in this repository are distributed free of charge from their authors under MIT or CC licenses. The only exception is MicroMage, which I personally own: thus you are NOT allowed to redistribute it or use it in any way.