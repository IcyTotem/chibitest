﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class InventoryCondition : MonoBehaviour, ICondition
{
    [SerializeField]
    [Tooltip("If true and there are required items, this trigger will fire if the player does NOT have such items. Otherwise it will fire if he DOES have them")]
    private bool triggerIfNoItems = false;

    [SerializeField]
    [Tooltip("List of items needed to activate")]
    private string[] requiredItems = new string[0];

    public bool IsSatisfied
    {
        get
        {
            if (this.requiredItems.Length == 0)
                return this.triggerIfNoItems;

            if (this.triggerIfNoItems)
                // The trigger requires the player to NOT have specified items, but the player DOES have them
                return (Player.Character.Inventory.Count == 0)
                    || (Player.Character.Inventory.Intersect(this.requiredItems).Count() == 0);
            else
                // The trigger requires the player to HAVE specified items, but the player does NOT have them
                return (Player.Character.Inventory.Intersect(this.requiredItems).Count() > 0);
        }
    }
}
