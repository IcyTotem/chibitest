﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class EnterTriggerCondition : MonoBehaviour, ICondition
{
    public bool IsSatisfied { get; private set; }

    void OnTriggerEnter(Collider other)
    {
        if (Player.Is(other.gameObject))
            this.IsSatisfied = true;
    }

    void OnTriggerExit(Collider other)
    {
        this.IsSatisfied = false;
    }
}
