﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This condition is valid only once per game session.
/// </summary>
public class OneTimeCondition : MonoBehaviour, ICondition
{
    private static IDictionary<string, bool> globalSwitches = new Dictionary<string, bool>();

    [SerializeField]
    [Tooltip("Arbitrary string used as id to persist this condition state between scenes")]
    private string id;

	void Awake() 
    {
        if (string.IsNullOrEmpty(this.id))
            this.id = this.gameObject.name;

        if (!globalSwitches.ContainsKey(this.id))
            globalSwitches.Add(this.id, false);
	}

    public bool IsSatisfied
    {
        get 
        {
            var result = !globalSwitches[this.id];
            globalSwitches[this.id] = true;
            return result;
        }
    }
}
