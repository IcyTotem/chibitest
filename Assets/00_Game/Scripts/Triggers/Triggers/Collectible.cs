﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : Trigger 
{
    private new Renderer renderer;

    [SerializeField]
    [Tooltip("Items awarded to the player when he collects this object")]
    private string[] collectedItems = new string[0];

    [SerializeField]
    [Tooltip("Items consumed by the player when he collects this object")]
    private string[] consumedItems = new string[0];


    protected override void Awake()
    {
        base.Awake();
        this.renderer = this.GetComponent<Renderer>();
    }

    protected override void Fire()
    {
        // Base class will handle animation and sound logic, and will invoke the
        // InteractionResolved method even if there is nothing to be played
        base.Fire();

        foreach (var item in this.consumedItems)
            Player.Character.Inventory.Remove(item);

        foreach (var item in this.collectedItems)
            Player.Character.Inventory.Add(item);

        this.enabled = false;
        
        if (this.renderer != null)
            this.renderer.enabled = false;
    }

    protected override void InteractionResolved()
    {
        this.gameObject.SetActive(false);
    }
}
