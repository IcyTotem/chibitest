﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;

[RequireComponent(typeof(Collider))]
public class TutorialTrigger : Trigger 
{   
    private Text text;

    #region Editable fields
    [SerializeField]
    [Tooltip("Reference to the UI object that contains the tutorial element to show/hide")]
    private GameObject tutorialContainer;

    [SerializeField]
    [Tooltip("Reference to the UI element representing the text to display")]
    private GameObject tutorialText;

    [SerializeField]
    [Tooltip("Text to be displayed when the player enters the trigger")]
    private string message;

    [Space]
    [SerializeField]
    [Tooltip("If true, time will slow down and stop as the player enters the trigger")]
    private bool slowTime = true;

    [Range(0, 5)]
    [SerializeField]
    [Tooltip("Amount of realtime it takes for the gametime to stop, measured in seconds")]
    private float slowFadeDuration = 1;

    [Range(1, 20)]
    [SerializeField]
    [Tooltip("Amount of realtime that the gametime will remain frozen after resuming")]
    private float slowDuration = 4;
    #endregion


	protected override void Awake()
    {
 	    base.Awake();
        this.text = tutorialText.GetComponent<Text>();
    }

    protected override void Fire()
    {
 	    base.Fire();

        this.enabled = false;

        this.StartCoroutine(this.SlowTime());
    }


    private IEnumerator SlowTime()
    {
        for (int i = 0; i <= 10; ++i)
        {
            Time.timeScale = (10 - i) / 10.0f;
            yield return new WaitForSecondsRealtime(this.slowFadeDuration / 10);
        }

        this.tutorialContainer.SetActive(true);
        this.text.text = this.message;
        yield return new WaitForSecondsRealtime(this.slowDuration);
        this.tutorialContainer.SetActive(false);

        for (int i = 0; i <= 10; ++i)
        {
            Time.timeScale = i / 10.0f;
            yield return new WaitForSecondsRealtime(this.slowFadeDuration / 10);
        }

        this.gameObject.SetActive(false);
    }
}
