﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockAttackTrigger : Trigger 
{
    [SerializeField]
    [Tooltip("Amount of mana that the trigger effect gives the player to enable his attacks")]
    private float restoredMana = 70;
    
    protected override void Fire()
    {
        base.Fire();

        this.enabled = false;

        Player.Character.CanAttack = true;
        Player.Character.MaxMana = this.restoredMana;
        Player.Character.CurrentMana = Player.Character.MaxMana;
    }
}
