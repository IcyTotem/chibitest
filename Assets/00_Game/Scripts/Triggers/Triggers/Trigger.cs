﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// Represents a generic volume of world space that triggers an action when the player steps into it.
/// </summary>
public abstract class Trigger : MonoBehaviour
{
    private ICondition[] conditions;
    private AudioSource audioSource;

    #region Editable fields
    [SerializeField]
    [Tooltip("If true, the player will perform the Interaction animation when ENTERING the volume")]
    private bool playAnimationOnEnter = false;

    [SerializeField]
    [Tooltip("If true, this will play the target audio clip when the player ENTERS the volume")]
    private bool playClipOnEnter = false;

    [SerializeField]
    [Tooltip("Audio clip to play when playClipOnEnter is true")]
    private AudioClip audioClip;
    #endregion


    protected virtual void Awake()
    {
        this.audioSource = this.GetComponent<AudioSource>();
        this.conditions = this.GetComponents<ICondition>();
    }

    protected virtual void Update()
    {
        if ((this.conditions.Length > 0) && this.conditions.All(condition => condition.IsSatisfied))
            this.Fire();
    }

	
	protected virtual void Fire()
    {
        this.enabled = false;

        if (this.playAnimationOnEnter)
            Player.Character.PlayInteractionAnimation();

        // Wait for any audio clip to finish before resolving (because this might disable this
        // gameobject and hence the audio source that is playing the clip)
        if (this.playClipOnEnter && this.audioSource && this.audioClip)
        {
            this.audioSource.PlayOneShot(this.audioClip);
            this.PerformDelayedTask(this.audioClip.length, this.InteractionResolved);
        }
        else
        {
            this.InteractionResolved();
        }
    }

    protected virtual void InteractionResolved()
    {
        // Overridden in derived classes
    }
}
