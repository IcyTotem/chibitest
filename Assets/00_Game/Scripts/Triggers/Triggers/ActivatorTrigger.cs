﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Repreents a volume trigger that activates another target objects when triggered.
/// </summary>
[RequireComponent(typeof(Collider))]
public class ActivatorTrigger : Trigger
{
    #region Editable fields
    [Space]
    [Range(0, 10)]
    [SerializeField]
    [Tooltip("Delay (in seconds) after which the trigger activates its effect, once a collision with the player has been detected")]
    private float delay = 0;

    [SerializeField]
    [Tooltip("Reference to the target object to be activated")]
    private GameObject target;

    [SerializeField]
    [Tooltip("If true, the target will be activated when the trigger fires. Otherwise, the target will be deactivated")]
    private bool forceActive = true;
    #endregion


	void Start()
    {
	    if (target == null)
        {
            this.enabled = false;
            Debug.LogWarning(this.name + " was disabled becase it has no target object");
        }
	}


    protected override void Fire()
    {
        base.Fire();

        this.enabled = false;

        if (this.delay > 0)
            this.PerformDelayedTask(this.delay, () => this.target.gameObject.SetActive(this.forceActive));
        else
            this.target.gameObject.SetActive(this.forceActive);
    }

}
