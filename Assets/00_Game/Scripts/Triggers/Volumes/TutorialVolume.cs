﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialVolume : Volume
{
    protected Text text;

    #region Editable fields
    [SerializeField]
    [Tooltip("Reference to the UI object that contains the tutorial element to show/hide")]
    private GameObject tutorialContainer;

    [SerializeField]
    [Tooltip("Reference to the UI element representing the text to display")]
    private GameObject tutorialText;

    [SerializeField]
    [Tooltip("Text to be displayed when the player enters the trigger")]
    private string message;
    #endregion


    protected override void Awake()
    {
        base.Awake();
        this.text = tutorialText.GetComponent<Text>();
    }

    protected override void Activate()
    {
        this.text.text = this.message;
        this.tutorialContainer.SetActive(true);
    }

    protected override void Deactivate()
    {
        this.tutorialContainer.SetActive(false);
    }
}
