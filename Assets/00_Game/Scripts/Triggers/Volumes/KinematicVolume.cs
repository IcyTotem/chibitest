﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Force the value of IsKinematic for the Rigidbody component of the player when he is inside the volume.
/// </summary>
public class KinematicVolume : Volume 
{
    [SerializeField]
    [Tooltip("Force the player box collider to be a trigger. This is DANGEROUS! It allows to avoid stuttering when exiting the area, but it may cause DEATH if the player character controller becomes inactive")]
    private bool makePlayerColliderTrigger = false;

    protected override void Activate()
    {
        var rigidbody = Player.GameObject.GetComponent<Rigidbody>();
        var collider = Player.GameObject.GetComponent<Collider>();

        if (rigidbody)
        {
            rigidbody.velocity = Vector3.zero;
            rigidbody.isKinematic = true;
        }

        if (collider && this.makePlayerColliderTrigger)
            collider.isTrigger = true;
    }

    protected override void Deactivate()
    {
        var rigidbody = Player.GameObject.GetComponent<Rigidbody>();
        var collider = Player.GameObject.GetComponent<Collider>();

        if (rigidbody)
        {
            rigidbody.isKinematic = false;
            rigidbody.velocity = Vector3.zero;
        }

        if (collider && this.makePlayerColliderTrigger)
            collider.isTrigger = false;
    }
}
