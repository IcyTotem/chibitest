﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformVolume : Volume 
{
    protected override void Activate()
    {
        Player.GameObject.transform.parent = this.gameObject.transform.parent;
    }

    protected override void Deactivate()
    {
        Player.GameObject.transform.parent = null;
    }
}
