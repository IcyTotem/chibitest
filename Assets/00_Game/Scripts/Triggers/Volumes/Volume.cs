﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(Collider))]
public abstract class Volume : MonoBehaviour 
{
    private ICondition[] conditions;
    

    protected virtual void Awake()
    {
        this.conditions = this.GetComponents<ICondition>();
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if (Player.Is(other.gameObject) && this.conditions.All(condition => condition.IsSatisfied))
            this.Activate();
    }

    protected virtual void OnTriggerExit(Collider other)
    {
        if (Player.Is(other.gameObject) && this.conditions.All(condition => condition.IsSatisfied))
            this.Deactivate();
    }


    protected abstract void Activate();
    protected abstract void Deactivate();
}
