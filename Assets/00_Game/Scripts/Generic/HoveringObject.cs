﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class HoveringObject : MonoBehaviour 
{
    private Vector3 startingPosition;

    #region Editable fields
    [Range(0, 50)]
    [SerializeField]
    [Tooltip("Angular speed of this object's rotation around the Y axis, measured in radians per second")]
    private float rotationSpeed = 2;

    [Range(0, 10)]
    [SerializeField]
    [Tooltip("Amount of time (in seconds) that the object takes to complete half of its floating trajectory along the Y axis")]
    private float floatingPeriod = 1;

    [Range(0, 50)]
    [SerializeField]
    [Tooltip("Amplitude (in units) of the floating motion")]
    private float floatingAmplitude = 2;
    #endregion


    void Start()
    {
        this.startingPosition = this.transform.position;
    }

	void Update() 
    {
        this.transform.RotateAround(Vector3.up, Time.deltaTime * this.rotationSpeed);
        this.transform.position = this.startingPosition + this.floatingAmplitude * Vector3.up * Mathf.Sin(Time.timeSinceLevelLoad / this.floatingPeriod * Mathf.PI);
	}
}
