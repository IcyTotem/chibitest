﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// This behaviour is similar to Patroller, but for inanimate objects.
/// <see cref="Patroller"/>
/// </summary>
public class MovingObject : MonoBehaviour
{
    private int lastWaypointIndex = -1;
    private float haltTimer = 0;

    #region Editable fields
    [Range(0, 3)]
    [SerializeField]
    [Tooltip("Maximum distance (in units) allowed for the object to consider himself arrived at the target waypoint")]
    private float precision = 0.5f;

    [Range(1, 30)]
    [SerializeField]
    [Tooltip("Speed at which the object travels, measured in units per second")]
    private float speed = 5f;

    [Range(0, 30)]
    [SerializeField]
    [Tooltip("Time spent at the waypoint position before departing again, measured in seconds")]
    private float haltTime = 5f;

    [SerializeField]
    [Tooltip("References to the game objects that will represent the target waypoints for this object")]
    private GameObject[] waypoints;

    [SerializeField]
    [Tooltip("Indices of the waypoints at which the object should halt and wait. If empty, the object will stop at all waypoint")]
    private int[] haltingIndices = new int[0];
    #endregion


    void Update()
    {
        // The object is halting at position for haltTime seconds
        if (this.haltTimer > 0)
        {
            this.haltTimer -= Time.deltaTime;
            return;
        }

        var nextWaypointIndex = (this.lastWaypointIndex + 1) % waypoints.Length;
        var nextWaypoint = this.waypoints[nextWaypointIndex];

        var motionVector = Vector3.Normalize(nextWaypoint.transform.position - this.transform.position);
        this.transform.position += motionVector * this.speed * Time.deltaTime;

        if (Vector3.Distance(this.transform.position, nextWaypoint.transform.position) <= this.precision)
        {
            if ((this.haltingIndices.Length == 0) 
                || (Array.IndexOf(this.haltingIndices, nextWaypointIndex) > -1) 
                || (this.lastWaypointIndex < 0))
                this.haltTimer = this.haltTime;
            this.lastWaypointIndex = nextWaypointIndex;
        }
    }
}
