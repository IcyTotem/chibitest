﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public sealed class HarmfulObject : MonoBehaviour
{
    #region Editable fields
    [SerializeField]
    [Tooltip("If true, the object will knock the player back when colliding with him")]
    private bool hasKnockbackEffect = true;

    [Range(100, 1000)]
    [SerializeField]
    [Tooltip("Magnitude of the impulsive force that will be applied to the player's rigidbody upon knockback")]
    private float knockbackMagnitude = 200;

    [Range(1, 200)]
    [SerializeField]
    [Tooltip("Amount of damage the player will receive because of this object")]
    private float damage = 10;
    #endregion

    void OnTriggerEnter(Collider other)
    {
        if (!Player.Is(other.gameObject) || !this.enabled)
            return;

        if (this.hasKnockbackEffect)
            Player.Character.Knockback(this.knockbackMagnitude, Player.GameObject.transform.position - this.transform.position);

        Player.Character.TakeDamage(this.damage);
    }
}
