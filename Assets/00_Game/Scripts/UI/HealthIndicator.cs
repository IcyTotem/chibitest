﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class HealthIndicator : MultiSliderIndicator 
{
    protected override float GetCurrentValue()
    {
        return Player.Character.CurrentHealth;
    }

    protected override float GetMaxValue()
    {
        return Player.Character.MaxHealth;
    }
}
