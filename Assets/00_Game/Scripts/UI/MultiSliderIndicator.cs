﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handles a generic float value display on the main UI. The value is represented by various
/// sliders that fill up or empty as the target value changes.
/// </summary>
public abstract class MultiSliderIndicator : MonoBehaviour 
{
    private float lastValue;
    private float valuePerSlider;
    private Slider[] sliders;

    #region Editable fields
    [Range(1, 20)]
    [SerializeField]
    [Tooltip("Rate at which sliders fill up when the target value changes")]
    private float fillingSpeed = 5;

    [SerializeField]
    [Tooltip("Collection of UI sliders representing the series of elements to fill")]
    private GameObject[] chunks;
    #endregion


    protected virtual void Awake()
    {
        this.sliders = new Slider[this.chunks.Length];
        for (var i = 0; i < chunks.Length; ++i)
            this.sliders[i] = this.chunks[i].GetComponent<Slider>();
    }

    protected virtual void Start()
    {
        this.valuePerSlider = this.GetMaxValue() / this.chunks.Length;
        this.lastValue = this.GetMaxValue();

        foreach (var slider in this.sliders)
            slider.value = this.valuePerSlider > 0 ? 1 : 0;
    }

    protected virtual void Update()
    {
        this.valuePerSlider = this.GetMaxValue() / this.chunks.Length;

        if (Mathf.Abs(this.lastValue - this.GetCurrentValue()) > 1)
        {
            this.lastValue += Time.deltaTime * fillingSpeed * Mathf.Sign(this.GetCurrentValue() - this.lastValue);

            for (int i = 0; i < sliders.Length; ++i)
            {
                var lowerBound = i * this.valuePerSlider;
                var upperBound = lowerBound + this.valuePerSlider;

                if (this.lastValue < lowerBound)
                    sliders[i].value = 0;
                else if (this.lastValue > upperBound)
                    sliders[i].value = 1;
                else
                    sliders[i].value = (this.lastValue - lowerBound) / this.valuePerSlider;
            }
        }
    }


    protected abstract float GetCurrentValue();
    protected abstract float GetMaxValue();
}
