﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class ManaIndicator : MultiSliderIndicator 
{
    protected override float GetCurrentValue()
    {
        return Player.Character.CurrentMana;
    }

    protected override float GetMaxValue()
    {
        return Player.Character.MaxMana;
    }
}
