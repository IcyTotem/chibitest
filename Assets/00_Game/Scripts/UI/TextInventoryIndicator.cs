﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class TextInventoryIndicator : MonoBehaviour
{
    private Text text;

    [SerializeField]
    [Tooltip("Reference to the UI object that contains the inventory element to show/hide")]
    private GameObject inventoryContainer;

    [SerializeField]
    [Tooltip("Reference to the UI element representing the text to display")]
    private GameObject inventoryText;

	void Awake()
    {
        this.text = this.inventoryText.GetComponent<Text>();
    }

    void FixedUpdate()
    {
        if (Player.Character.Inventory.Count == 0)
        {
            this.inventoryContainer.SetActive(false);
        }
        else
        {
            this.inventoryContainer.SetActive(true);

            var builder = new StringBuilder();

            foreach (var item in Player.Character.Inventory)
            {
                if (builder.Length > 0)
                    builder.Append(", ");
                builder.Append(item);
            }

            this.text.text = builder.ToString();
        }
    }
}
