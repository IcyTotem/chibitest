﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A projectile is a small fast moving object that travels to a destination. With the current implementation,
/// the projectile always reaches its target despite of the direction it was fired towards. Also, it does
/// not use raycasting since it is usually associated with a flashy particle effect.
/// </summary>
public sealed class Projectile : MonoBehaviour 
{
    private bool ready = true;
    private GameObject target;

    #region Editable fields
    [SerializeField]
    [Tooltip("Reference to the game object at whose position this projectile must spawn. For example, the character hand, for a fireball")]
    private GameObject spawner;

    [Range(5, 100)]
    [SerializeField]
    [Tooltip("Speed at which this projectile travels, measured in units per second")]
    private float speed = 20;

    [Range(1, 200)]
    [SerializeField]
    [Tooltip("Absolute damage that the projectile will cause when hitting the target character. Each character may react to damage differently")]
    private float damage = 10;
    #endregion


    public float Damage
    {
        get { return this.damage; }
    }

    public bool IsReady
    {
        get { return this.ready; }
        set { this.ready = value; }
    }


	void FixedUpdate() 
    {
        if (this.target == null)
            return;

        var distance = Vector3.Distance(this.transform.position, this.target.transform.position);
        var threshold = this.speed * Time.fixedDeltaTime;

        // The task of chacking for incoming projectile is delegated to the target object
        if (distance > threshold)
        {
            var motionVector = Vector3.Normalize(this.target.transform.position - this.transform.position);
            this.transform.position += motionVector * speed * Time.deltaTime;
        }
	}

    /// <summary>
    /// Release this projectile towards the given target. It will run at given speed until it reaches it.
    /// The projectile will follow its target everywhere until it's near enough.
    /// </summary>
    public void FireTowards(GameObject target)
    {
        this.IsReady = false;
        this.gameObject.SetActive(true);
        this.transform.parent = null;
        this.target = target;
    }

    /// <summary>
    /// Reattach this projectile to its spawn point.
    /// </summary>
    public void Return()
    {
        this.gameObject.SetActive(false);
        this.transform.parent = this.spawner.transform.parent;
        this.transform.localPosition = Vector3.zero;
        this.IsReady = true;
    }
}
