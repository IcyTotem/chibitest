﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator), typeof(CharacterController))]
public abstract class CharacterBehaviour : MonoBehaviour
{
    protected CharacterController characterController;
    protected Animator animator;
    protected AudioSource audioSource;
    protected new Rigidbody rigidbody;
    protected new Collider collider;

    protected bool frozen = false;

    protected virtual void Awake()
    {
        this.rigidbody = this.GetComponent<Rigidbody>();
        this.characterController = this.GetComponent<CharacterController>();
        this.animator = this.GetComponent<Animator>();
        this.collider = this.GetComponent<Collider>();
        this.audioSource = this.GetComponent<AudioSource>();
    }

    /// <summary>
    /// Force the current character to start an animation and play through it without being able to stop or move in the meanwhile.
    /// </summary>
    /// <param name="triggerName">Name of the Mecanim trigger parameter that will cause the animation FSM to enter the desired
    /// animation state.</param>
    /// <param name="animationClip">This is purely used to know the clip duration in seconds in order to restore
    /// controls of the character once the animation has ended. Far more practical than StateMachineBehaviour.</param>
    public void PlayInstantUniterruptibleAnimation(string triggerName, AnimationClip animationClip)
    {
        this.Freeze();
        this.animator.SetTrigger(triggerName);
        this.PerformDelayedTask(animationClip.length, this.Unfreeze);
    }

    public void PlayInstantUniterruptibleAnimation(string triggerName, AnimationClip animationClip, AudioClip audioClip)
    {
        this.PlayInstantUniterruptibleAnimation(triggerName, animationClip);

        if (this.audioSource && audioClip)
            this.audioSource.PlayOneShot(audioClip);
    }

    /// <summary>
    /// Stop the character motion and inhibits its character controller. This is used for example to
    /// play uniterruptible animations or to stop the player during events.
    /// </summary>
    public virtual void Freeze()
    {
        if (this.rigidbody != null)
            this.rigidbody.velocity = Vector3.zero;

        this.frozen = true;
        this.characterController.enabled = false;
    }

    /// <summary>
    /// Opposite method of Freeze.
    /// </summary>
    public virtual void Unfreeze()
    {
        this.frozen = false;
        this.characterController.enabled = true;
    }

    public static class AnimationParams
    {
        public const string Walk = "Walk";
        public const string GetHit = "Hit";
        public const string HitPlayer = "HitPlayer";
        public const string ActivateItem = "ActivateItem";
        public const string Death = "Death";
        public const string Attack = "Attack";
        public const string InvalidAction = "InvalidAction";
        public const string Revive = "Revive";
    }
}
