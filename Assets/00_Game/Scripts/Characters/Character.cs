﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A Character component represents the main view of a character associated to a game object.
/// It is meant to contain fields like health, damage, mana and all other statistics that makes
/// the character unique from the others; such statistics are high-level specifications of the
/// character skills and can be explained easily during the design phase.
/// Subclasses also contains the main logic that allows the character to interact with the world
/// and with other characters.
/// </summary>
public class Character : CharacterBehaviour
{
    #region Editable fields
    [Range(1, 1000)]
    [SerializeField]
    [Tooltip("Maximum health points of this character (and thus starting health pool at level load)")]
    private float maxHealth = 100;
    
    [Range(1, 1000)]
    [SerializeField]
    [Tooltip("Current health points of this character")]
    private float currentHealth = 100;

    [SerializeField]
    [Tooltip("Animation reference to the clip played by Mecanim when the character takes damage (Hit trigger)")]
    private AnimationClip takeDamageAnimation;

    [SerializeField]
    [Tooltip("Sound played during the TakeDamage animation. It can be null: in that case, no sound will be played")]
    private AudioClip takeDamageSound;
    #endregion


    public float CurrentHealth
    {
        get { return this.currentHealth; }
        set { this.currentHealth = Mathf.Clamp(value, 0, this.maxHealth); }
    }

    public float MaxHealth
    {
        get { return this.maxHealth; }
        set 
        { 
            this.maxHealth = Mathf.Max(0, value);
            this.currentHealth = Mathf.Min(this.currentHealth, value); 
        }
    }

    /// <summary>
    /// True iif the character is currently dead. This property becomes true as soon as CurrentHealth reaches 0,
    /// therefore it may indicate a character is dead before the Death animation has finished.
    /// </summary>
    public bool IsDead
    {
        get { return this.CurrentHealth <= 0; }
    }


    protected virtual void Start()
    {
        this.CurrentHealth = this.maxHealth;
    }


    /// <summary>
    /// Instantly bring this character's health to 0, effectively killing it. It will also trigger the Death animation
    /// (if any) and disable all components implementing IMortalBehaviour. Additionally, the character will automatically
    /// despawn after two seconds from the animation end.
    /// </summary>
    public virtual void Die()
    {
        this.currentHealth = 0;
        this.collider.enabled = false;
        this.characterController.enabled = false;

        foreach (var mortalBehaviour in this.GetComponents<IMortalBehaviour>())
            mortalBehaviour.OnDeath();

        this.animator.SetTrigger(AnimationParams.Death);
    }

    /// <summary>
    /// Take the given amount of damage, which will be subtracted from CurrentHealth. Damage can be negative, but in no
    /// case will it reduce CurrentHealth below 0. If CurrentHealth reaches 0, this will trigger the character death
    /// as when Die() is called; otherwise it will begin the TakeDamage animation (Hit trigger) and possibly play its
    /// associated sound.
    /// </summary>
    /// <param name="amount">Amount of damage to take.</param>
    public virtual void TakeDamage(float amount)
    {
        this.CurrentHealth = Mathf.Max(0, this.CurrentHealth - amount);

        if (this.CurrentHealth <= 0)
            this.Die();
        else
            this.PlayInstantUniterruptibleAnimation(AnimationParams.GetHit, this.takeDamageAnimation, this.takeDamageSound);
    }


    /// <summary>
    /// Called by Mecanim when the Death animation is finished.
    /// </summary>
    protected virtual void Mecanim_Dead()
    {
        this.FadeOut(2);
    }
}
