﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class HarmfulCreature : CharacterBehaviour, IMortalBehaviour
{
    private float lastAttackTime = 0;
    private bool playerCurrentlyInsideCollider = false;

    #region Editable fields
    [SerializeField]
    [Tooltip("If true, the creature will knock the player back when its attack hits him")]
    private bool hasKnockbackEffect = true;

    [Range(100, 1000)]
    [SerializeField]
    [Tooltip("Magnitude of the impulsive force that will be applied to the player's rigidbody upon knockback")]
    private float knockbackMagnitude = 200;

    [Range(1, 200)]
    [SerializeField]
    [Tooltip("Amount of damage the player will receive because of this creature's attack")]
    private float damage = 10;

    [Range(0, 10)]
    [SerializeField]
    [Tooltip("Minimum amount of time (in seconds) that must pass after an attack for this creature to try and attack again")]
    private float timeBetweenAttacks = 2;

    [SerializeField]
    [Tooltip("Animation clip (uniterruptible) that will be played by this creature's animator upon entering the attack state")]
    private AnimationClip attackAnimation;

    [SerializeField]
    [Tooltip("Audio clip associated to the attack animation. It can be empty if a sound is not desired")]
    private AudioClip attackSound;
    #endregion


    void OnTriggerEnter(Collider other)
    {
        if (!Player.Is(other.gameObject) || !this.enabled)
            return;

        // Avoid hitting the player more than once
        if (!this.playerCurrentlyInsideCollider && (Time.timeSinceLevelLoad - this.lastAttackTime > this.timeBetweenAttacks))
        {
            this.lastAttackTime = Time.timeSinceLevelLoad;
            this.PlayInstantUniterruptibleAnimation(CharacterBehaviour.AnimationParams.HitPlayer, attackAnimation, attackSound);
        }

        this.playerCurrentlyInsideCollider = true;
    }

    void OnTriggerExit(Collider other)
    {
        if (!Player.Is(other.gameObject) || !this.enabled)
            return;

        this.playerCurrentlyInsideCollider = false;
    }

    /// <summary>
    /// Called by Mecanim when the Hit animation is in the right spot for the player to start receiving damage.
    /// </summary>
    void Mecanim_HitPlayer()
    {
        if (this.playerCurrentlyInsideCollider)
        {
            if (this.hasKnockbackEffect)
                Player.Character.Knockback(this.knockbackMagnitude, Player.GameObject.transform.position - this.transform.position);

            Player.Character.TakeDamage(this.damage);
        }
    }

    public void OnDeath()
    {
        this.enabled = false;
    }
}
