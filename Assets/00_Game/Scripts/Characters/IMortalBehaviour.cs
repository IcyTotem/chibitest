﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This represents a component that is only active when the character to whom it is attached
/// is alive, and should be disabled on death.
/// </summary>
public interface IMortalBehaviour
{
    void OnDeath();
}
