﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A patroller is a character that perpetually walks a predefined route. The route is marked with
/// several milestones called waypoints. A patroller always attempts to travel to its waypoints in
/// order from the first to the last, the returns to the first one and starts its patrolling route again.
/// </summary>
public sealed class Patroller : CharacterBehaviour, IMortalBehaviour
{
    private Vector3 lastNonzeroMotionVector;
    private int lastWaypointIndex = -1, nextWaypointIndex = 0;
    private bool lastWalkingFlag = false;

    #region Editable fields
    [Range(0.5f, 3)]
    [SerializeField]
    [Tooltip("Maximum distance (in units) allowed for the patroller to consider himself arrived at the target waypoint")]
    private float precision = 0.5f;

    [Range(1, 30)]
    [SerializeField]
    [Tooltip("Speed at which the patroller travels, measured in units per second")]
    private float speed = 5f;

    [Range(1, 10)]
    [SerializeField]
    [Tooltip("Angular speed at which the patroller turns when changing direction, measured in radians per second")]
    private float turningSpeed = 5;

    [SerializeField]
    [Tooltip("If true, the patroller will smoothly turn around to face the next waypoint")]
    private bool smoothTurn = true;

    [SerializeField]
    [Tooltip("References to the game objects that will represent the target waypoints for this patroller")]
    private GameObject[] waypoints;
    #endregion


    void Start()
    {
        this.animator.speed = Mathf.Pow(this.speed, 0.75f);
    }

    void Update()
    {
        if (!this.characterController.enabled)
        {
            if (this.lastWalkingFlag == true)
            {
                this.lastWalkingFlag = false;
                this.animator.SetBool(AnimationParams.Walk, false);
            }

            return;
        }

        var nextWaypoint = this.waypoints[nextWaypointIndex];
        var motionVector = Vector3.Normalize(nextWaypoint.transform.position - this.transform.position);
        this.characterController.SimpleMove(motionVector * speed);

        if (motionVector != Vector3.zero)
            this.lastNonzeroMotionVector = motionVector;

        if (Vector3.Distance(this.transform.position, nextWaypoint.transform.position) <= this.precision)
        {
            this.lastWaypointIndex = this.nextWaypointIndex;
            this.nextWaypointIndex = (this.lastWaypointIndex + 1) % waypoints.Length;
        }

        // The dot condition avoid the patroller to turn itself upside down when rotating steeply
        if (this.smoothTurn)
        {
            var turnCoef = Vector3.Dot(this.transform.forward, this.lastNonzeroMotionVector);
            if ((turnCoef > -0.7f) && (turnCoef < 0.95f))
                this.transform.forward = Vector3.RotateTowards(this.transform.forward, this.lastNonzeroMotionVector, this.turningSpeed * Time.deltaTime, 0.0f);
            else
                this.transform.forward = this.lastNonzeroMotionVector;
        }
        else
        {
            this.transform.forward = this.lastNonzeroMotionVector;
        }

        if (this.lastWalkingFlag == false)
        {
            this.lastWalkingFlag = true;
            this.animator.SetBool(AnimationParams.Walk, true);
        }
    }


    public void OnDeath()
    {
        this.enabled = false;
    }
}