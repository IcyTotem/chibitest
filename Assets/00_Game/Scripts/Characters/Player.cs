﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Represents the Player. Holds the player's main parameters like speed, mana, health, etc...
/// and additionally acts as a controller for all human inputs and animations.
/// </summary>
public sealed class Player : Character
{
    private GameObject[] targettableEnemies;
    private Vector3 lastNonzeroMotionVector = Vector3.right;
    private GameObject lastTargettedEnemy;
    private Projectile fireballProjectile;
    private bool collidingWithSomething = false;

    #region Editable fields
    [Space]
    [Range(1, 30)]
    [SerializeField]
    [Tooltip("Speed at which the player moves, measured in units per second")]
    private float speed = 5f;

    [Range(1, 10)]
    [SerializeField]
    [Tooltip("Angular speed at which the player turns when changing direction, measured in radians per second")]
    private float turningSpeed = 5;

    [Space]
    [SerializeField]
    [Tooltip("Direct reference to the game object represnting the player throwable fireball")]
    private GameObject fireball;

    [SerializeField]
    [Tooltip("Audio clip to play when the player launches its fireball. Can be empty if no sound is desired")]
    private AudioClip fireballSound;

    [Range(1, 100)]
    [SerializeField]
    [Tooltip("Mana amount that the player spends when casting a single fireball")]
    private float fireballManaCost = 15;

    [Range(10, 200)]
    [SerializeField]
    [Tooltip("Maximum distance the player can can his spells to. If an enemy is farther than this value (in units), no spell will be cast")]
    private float spellReachRadius = 10;

    [SerializeField]
    [Tooltip("True to enable offensive actions (spells)")]
    private bool canAttack = false;

    [Range(0, 1000)]
    [SerializeField]
    [Tooltip("Maximum amount of mana available to the player")]
    private float maxMana = 100;

    [Range(0, 1000)]
    [SerializeField]
    [Tooltip("Current level of mana available to the player")]
    private float currentMana = 100;

    [Space]
    [SerializeField]
    [Tooltip("Animation clip to play when the player interacts with active game objects or triggers (uniterruptible)")]
    private AnimationClip interactionAnimation;

    [SerializeField]
    [Tooltip("Animation clip to play when the player attempts to perform an invalid action (e.g. casting a spell without mana)")]
    private AnimationClip invalidActionAnimation;

    [Space]
    [SerializeField]
    [Tooltip("Sound played by the player dies")]
    private AudioClip deathSound;

    [SerializeField]
    [Tooltip("When the player falls below this height in world space, he atomatically dies")]
    private float deathY = -10;

    [SerializeField]
    [Tooltip("Reference to the empty object where the player respawns on death")]
    private GameObject spawnPoint;

    [Range(0, 5)]
    [SerializeField]
    [Tooltip("Time (in seconds) that the player has to wait after dying before respawning")]
    private float respawnDelay = 1.5f;
    #endregion


    public float CurrentMana
    {
        get { return this.currentMana; }
        set { this.currentMana = Mathf.Clamp(value, 0, this.maxMana); }
    }

    public float MaxMana
    {
        get { return this.maxMana; }
        set
        {
            this.maxMana = Mathf.Max(0, value);
            this.currentMana = Mathf.Min(this.currentMana, value);
        }
    }

    public bool CanAttack
    {
        get { return this.canAttack; }
        set { this.canAttack = value; }
    }

    public bool IsGrounded
    {
        get 
        {
            // For some reason, when using a box collider, it consider the transform position to be below the terrain...
            // I cannot fathom why
            return Physics.Raycast(this.transform.position, -Vector3.up, this.collider.bounds.extents.y / 2)
                || Physics.Raycast(this.transform.position + Vector3.up, -Vector3.up, 1 + this.collider.bounds.extents.y / 2);
        }
    }

    public IList<string> Inventory { get; private set; }

    public static GameObject GameObject { get; private set; }

    public static Player Character { get; private set; }


    protected override void Awake()
    {
        base.Awake();
        this.targettableEnemies = GameObject.FindGameObjectsWithTag("TargettableEnemy");
        this.fireballProjectile = fireball.GetComponent<Projectile>();

        this.Inventory = new List<string>();

        Player.GameObject = this.gameObject;
        Player.Character = this;
    }

    protected override void Start()
    {
        base.Start();
        this.CurrentMana = this.maxMana;
    }

    void Update()
    {
        if (!this.characterController.enabled)
            return;

        var verticalMotion = Input.GetAxisRaw("Vertical");
        var horizontalMotion = Input.GetAxisRaw("Horizontal");

        // Setting y = 0 avoid our player to eat the dust
        var right = Camera.main.transform.right;
        right.y = 0;
        right.Normalize();

        var forward = Camera.main.transform.forward;
        forward.y = 0;
        forward.Normalize();

        var motionVector = Vector3.Normalize(right * horizontalMotion + forward * verticalMotion);
        this.characterController.SimpleMove(motionVector * speed);

        if (motionVector != Vector3.zero)
            this.lastNonzeroMotionVector = motionVector;

        this.transform.forward = Vector3.RotateTowards(this.transform.forward, this.lastNonzeroMotionVector, this.turningSpeed * Time.deltaTime, 0.0f);
        this.animator.SetBool(AnimationParams.Walk, (verticalMotion != 0.0f) || (horizontalMotion != 0.0f));

        if (Input.GetButtonDown("Fire1"))
            this.ThrowFireball();
    }

    void FixedUpdate()
    {
        // This is used to reenable the character controller after a knockback
        if (!this.frozen && !this.IsDead && (this.IsGrounded || this.collidingWithSomething))
            this.characterController.enabled = true;

        if ((this.transform.position.y < this.deathY) && !this.IsDead)
            this.Die();
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, this.spellReachRadius);
    }

    void OnCollisionEnter(Collision collision)
    {
        this.collidingWithSomething = true;
    }

    void OnCollisionExit(Collision collision)
    {
        this.collidingWithSomething = false;
    }


    /// <summary>
    /// Attempt to throw a fireball at the nearest facing enemy. If an enemy exists, the attack animation
    /// is played (which will trigger the actual projectile launch through a Mecanim event). If an enemy
    /// doesn't exist, it plays a generic confusion animation.
    /// The action will be successful only if the player can attack, the fireball projectile is ready and
    /// the player has enough mana to cast it.
    /// </summary>
    private void ThrowFireball()
    {
        if (!this.canAttack || !this.fireballProjectile.IsReady)
            return;

        this.lastTargettedEnemy = this.FindNearestEnemy();

        if ((this.lastTargettedEnemy == null) || (this.CurrentMana < this.fireballManaCost))
        {
            this.PlayInstantUniterruptibleAnimation(AnimationParams.InvalidAction, this.invalidActionAnimation);
        }
        else
        {
            this.CurrentMana = Mathf.Max(0, this.CurrentMana - this.fireballManaCost);
            this.fireballProjectile.IsReady = false;
            this.audioSource.PlayOneShot(fireballSound);
            this.animator.SetTrigger(AnimationParams.Attack);
        }
    }

    /// <summary>
    /// Find the nearest enemy in the direction the player is facing. The enemy must also be within
    /// spellReachRadius from the player.
    /// </summary>
    /// <returns>Return the enemy game object if one is found, or null is there are no suitable
    /// enemies nearby (no enemies, enemies are too far, or player is facing the wrong direction).</returns>
    private GameObject FindNearestEnemy()
    {
        float minDistance = float.PositiveInfinity;
        GameObject closestFacingEnemy = null;

        foreach (var enemy in targettableEnemies)
        {
            if ((enemy == null) || !enemy.activeSelf)
                continue;

            var character = enemy.GetComponent<Character>();

            if ((character == null) || character.IsDead)
                continue;

            float distance = Vector3.Distance(enemy.transform.position, this.transform.position);
            float facing = Vector3.Dot(this.transform.forward, enemy.transform.position - this.transform.position);

            if ((distance < minDistance) && (facing > 0))
            {
                minDistance = distance;
                closestFacingEnemy = enemy;
            }
        }

        if (minDistance <= this.spellReachRadius)
            return closestFacingEnemy;
        else
            return null;
    }


    public override void Freeze()
    {
        base.Freeze();

        // Avoid queueing up attack animations
        this.animator.ResetTrigger(AnimationParams.Attack);
        this.animator.SetBool(AnimationParams.Walk, false);
    }

    public override void Unfreeze()
    {
        this.frozen = false;
        if (this.IsGrounded || this.collidingWithSomething)
            this.characterController.enabled = true;
    }

    public override void Die()
    {
        this.Freeze();

        this.CurrentHealth = 0;
        this.collider.enabled = false;
        this.rigidbody.isKinematic = true;

        this.animator.SetTrigger(AnimationParams.Death);

        if (this.deathSound)
            this.audioSource.PlayOneShot(this.deathSound);

        var delay = this.deathSound ? this.deathSound.length : 0;
        this.PerformDelayedTask(delay + this.respawnDelay, this.Revive);
    }

    public void Revive()
    {
        this.CurrentHealth = this.MaxHealth;
        this.transform.position = this.spawnPoint.transform.position;
        this.characterController.enabled = true;
        this.Unfreeze();
        this.animator.SetTrigger(AnimationParams.Revive);
    }

    public void PlayInteractionAnimation()
    {
        this.PlayInstantUniterruptibleAnimation(AnimationParams.ActivateItem, this.interactionAnimation);
    }

    public static bool Is(GameObject other)
    {
        return (other.tag == "Player") && !Player.Character.IsDead;
    }


    /// <summary>
    /// This method is called by Mecanim when the Attack animation has reached the appropriate point in time.
    /// </summary>
    void Mecanim_LaunchFireball()
    {
        this.fireballProjectile.FireTowards(this.lastTargettedEnemy);
    }
}
