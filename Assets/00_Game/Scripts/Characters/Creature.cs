﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature : Character, IMortalBehaviour
{
    [Range(0, 5)]
    [SerializeField]
    [Tooltip("Indicates how much to wait (in seconds) after a projectile hit this creature before triggering TakeDamage")]
    private float projectileHitDelay = 0.7f;

	void OnTriggerEnter(Collider other)
    {
        if (this.IsDead)
            return;

        var projectile = other.GetComponent<Projectile>();

        if (projectile)
        {
            if (this.projectileHitDelay <= 0)
            {
                projectile.Return();
                this.TakeDamage(projectile.Damage);
            }
            else
            {
                this.PerformDelayedTask(this.projectileHitDelay, () =>
                {
                    projectile.Return();
                    this.TakeDamage(projectile.Damage);
                });
            }
        }
    }

    public void OnDeath()
    {
        // Nothing special for this script yet
    }
}
