﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowingWater : MonoBehaviour 
{
    private new Renderer renderer;

    public Vector2 period = 20f * Vector2.one;

	// Use this for initialization
	void Awake() 
    {
        this.renderer = this.GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        var tx = Time.timeSinceLevelLoad / period.x;
        var ty = Time.timeSinceLevelLoad / period.y;
	    this.renderer.material.SetTextureOffset("_MainTex", new Vector2(Mathf.Cos(tx * Mathf.PI), Mathf.Sin(ty * Mathf.PI)));
	}
}
