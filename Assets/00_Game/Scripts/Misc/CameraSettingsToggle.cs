﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class CameraSettingsToggle : MonoBehaviour 
{
    private DepthOfField depthOfField;

    void Awake()
    {
        this.depthOfField = this.GetComponent<DepthOfField>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
            this.depthOfField.enabled = !this.depthOfField.enabled;
    }
}
