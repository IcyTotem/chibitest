﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class Utils
{
	public static void PerformDelayedTask(this MonoBehaviour instance, float waitTime, Action task)
    {
        instance.StartCoroutine(Delay(waitTime, task));
    }

    public static void FadeOut(this MonoBehaviour instance, float time)
    {
        var renderer = instance.GetComponentInChildren<Renderer>();
        if (renderer != null)
            instance.StartCoroutine(FadeOutCoroutine(instance.gameObject, renderer, time));
        else
            instance.gameObject.SetActive(false);
    }

    public static void Knockback(this MonoBehaviour instance, float forceMagnitude, Vector3 direction)
    {
        var rigidbody = instance.GetComponent<Rigidbody>();

        if (rigidbody != null)
        {
            // It is necessary to halt this object in order to avoid clunky kockback animations
            rigidbody.velocity = Vector3.zero;
            instance.transform.position += Vector3.up;

            direction.y = 0;

            var impulse = Vector3.Normalize(direction);
            impulse.y = 1;

            rigidbody.AddForce(impulse * forceMagnitude);
        }
    }

    public static void Knockback(this MonoBehaviour instance, float forceMagnitude)
    {
        Knockback(instance, forceMagnitude, -instance.transform.forward);
    }

    private static IEnumerator Delay(float waitTime, Action task)
    {
        yield return new WaitForSeconds(waitTime);
        task.Invoke();
    }

    private static IEnumerator FadeOutCoroutine(GameObject rootObject, Renderer renderer, float time)
    {
        var tick = new WaitForSeconds(Time.deltaTime);
        var count = time / Time.deltaTime;
        var deltaAlpha = renderer.material.color.a / count;

        while (renderer.material.color.a > 0)
        {
            var color = renderer.material.color;
            renderer.material.SetColor("_Color", new Color(color.r, color.g, color.b, Mathf.Max(0, color.a - deltaAlpha)));
            yield return tick;
        }

        rootObject.SetActive(false);
    }
}
