﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedTargetCamera : MonoBehaviour
{
    private Vector3 startingMousePosition;
    private Vector3 positionVector;
    private Vector3 orientation, startingOrientation;
    private float elevation, startingElevation;
    private float distance, startingDistance;

    #region Editable fields
    [SerializeField]
    [Tooltip("Reference to the game object to follow")]
    private GameObject target;

    [SerializeField]
    [Tooltip("Sensitivity of the mouse movements, i.e. how many pixels correspond to a fixed angle sweep. Z component is zoom scale")]
    private Vector3 sensitivity = new Vector3(0.009f, 0.008f, 7);

    [Range(1, 10)]
    [SerializeField]
    [Tooltip("Minimum distance of the camera from the target")]
    private float minDistance = 3;

    [Range(2, 50)]
    [SerializeField]
    [Tooltip("Maximum distance of the camera from the target")]
    private float maxDistance = 10;
    #endregion

    
    public Vector3 LookVector
    {
        get { return this.positionVector; }
    }


	void Awake() 
    {
        var lookVector = this.target.transform.position - this.transform.position;
        this.distance = lookVector.magnitude;
        this.orientation = Vector3.ProjectOnPlane(Vector3.Normalize(lookVector), Vector3.up);
        this.elevation = Vector3.Angle(lookVector, this.orientation) * Mathf.PI / 180;
        this.positionVector = lookVector;
	}
	
    void Start()
    {
        this.transform.LookAt(this.target.transform);
    }

	void LateUpdate() 
    {
        this.transform.position = this.target.transform.position - this.positionVector;

        if (Input.GetButtonDown("Fire2"))
        {
            this.startingMousePosition = Input.mousePosition;
            this.startingOrientation = this.orientation;
            this.startingElevation = this.elevation;
            this.startingDistance = this.distance;
        }

        if (Input.GetButton("Fire2"))
        {
            var deltaVertical = -(Input.mousePosition.y - this.startingMousePosition.y) * this.sensitivity.y;
            var deltaHorizontal = (Input.mousePosition.x - this.startingMousePosition.x) * this.sensitivity.x;
            var deltaZoom = -Input.GetAxis("Mouse ScrollWheel");

            var deltaOrientation = Mathf.Atan(deltaHorizontal);
            var deltaElevation = Mathf.Atan(deltaVertical);

            this.orientation = Quaternion.EulerAngles(0, deltaOrientation, 0) * this.startingOrientation;
            this.elevation = Mathf.Max(0, this.startingElevation + deltaElevation);
            this.distance = Mathf.Clamp(this.distance + deltaZoom * this.sensitivity.z, this.minDistance, this.maxDistance);

            this.positionVector = Vector3.RotateTowards(this.orientation, Vector3.up, -elevation, 0) * this.distance;
            this.transform.LookAt(this.target.transform);
        }
	}


    public void ForceLookingDirection(Vector3 newDirection)
    {
    }
}
