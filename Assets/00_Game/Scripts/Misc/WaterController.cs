﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Water;

public class WaterController : MonoBehaviour 
{
    private WaterBase realisticWaterController;

    [SerializeField]
    private GameObject cartoonWater;

    [SerializeField]
    private GameObject realisticWater;

    [SerializeField]
    private WaterQuality quality;


	void Awake()
    {
        this.realisticWaterController = this.realisticWater.GetComponent<WaterBase>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            this.quality = (WaterQuality)(((int)this.quality + 1) % 4);
            this.ChangeWaterQuality(this.quality);
        }
    }

    private void ChangeWaterQuality(WaterQuality quality)
    {
        switch (quality)
        {
            case WaterQuality.Cartoon:
                this.realisticWater.SetActive(false);
                this.cartoonWater.SetActive(true);
                break;

            case WaterQuality.RealisticLow:
                this.cartoonWater.SetActive(false);
                this.realisticWater.SetActive(true);
                this.realisticWaterController.waterQuality = UnityStandardAssets.Water.WaterQuality.Low;
                break;

            case WaterQuality.RealisticMedium:
                this.cartoonWater.SetActive(false);
                this.realisticWater.SetActive(true);
                this.realisticWaterController.waterQuality = UnityStandardAssets.Water.WaterQuality.Medium;
                break;

            case WaterQuality.RealisticHigh:
                this.cartoonWater.SetActive(false);
                this.realisticWater.SetActive(true);
                this.realisticWaterController.waterQuality = UnityStandardAssets.Water.WaterQuality.High;
                break;
        }
    }


    public enum WaterQuality
    {
        Cartoon,
        RealisticLow,
        RealisticMedium,
        RealisticHigh
    }
}
